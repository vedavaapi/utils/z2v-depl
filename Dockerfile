FROM node:10
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -yq update
RUN apt-get -yq install nano git
RUN npm install --quiet -g pm2

ENV SRC_DIR /src/z2v
RUN mkdir -p $SRC_DIR
WORKDIR $SRC_DIR

ARG rebuild_flag
ARG version
RUN echo "version : ${version}" > version_info.txt
RUN git clone https://gitlab.com/vedavaapi/utils/z2v-ui.git
WORKDIR "${SRC_DIR}/z2v-ui"
RUN git checkout "${version}"
RUN git pull
RUN npm install --quiet

COPY zconfig.json ./config/production.json

RUN npm run build

EXPOSE ${PORT}
CMD ["pm2-runtime", "start", "__sapper__/build"]
